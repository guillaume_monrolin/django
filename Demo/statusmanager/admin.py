from django.contrib import admin
from statusmanager.models import Report, Table

admin.site.register(Report)
admin.site.register(Table)
