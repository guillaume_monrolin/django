from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.conf import settings
from django.core.management import BaseCommand
from django.forms.models import model_to_dict

from statusmanager.models import Report


class Command(BaseCommand):
    help = 'Command to start stream socket data if any socket open'

    def handle(self, *args, **options):
        group_name = settings.STREAM_SOCKET_GROUP_NAME
        channel_layer = get_channel_layer()
        reports = Report.objects.all()
        reports_formated = []
        for report in reports:
            report.task1 = (report.task1 + 1) % 3
            report.task2 = (report.task2 + 1) % 3
            report.task3 = (report.task3 + 1) % 3
            report.task4 = (report.task4 + 1) % 3

            reports_formated.append(model_to_dict(report))
        
        async_to_sync(channel_layer.group_send)(
            group_name,
            {
                'type': 'system_load',
                'data': reports_formated
            }
        )
