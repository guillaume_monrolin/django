from django.apps import AppConfig


class StatusmanagerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'statusmanager'
