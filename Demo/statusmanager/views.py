from django.http import HttpResponse
from django.shortcuts import render
from .models import Report
# Create your views here.


def index(request):
    reports = Report.objects.all()
    context = {'reports': reports}
    return render(request, template_name="statusmanager/index.html", context=context)
