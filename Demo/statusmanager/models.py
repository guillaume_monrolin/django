from django.db import models

# Create your models here.

STATUS = (
    (0, 'TODO'),
    (1, 'IN PROGRESS'),
    (2, 'DONE')
)


class Report(models.Model):
    task1 = models.IntegerField(default=0, choices=STATUS)
    task2 = models.IntegerField(default=0, choices=STATUS)
    task3 = models.IntegerField(default=0, choices=STATUS)
    task4 = models.IntegerField(default=0, choices=STATUS)


class Table(models.Model):
    nombreDePieds = models.IntegerField(default=4)
    matiereDePlateau = models.CharField(max_length=100)
